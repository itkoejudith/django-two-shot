from django.urls import path
from .views import (
    receipt_list,
    create_receipts,
    expense_view,
    accounts_view,
    expense_category,
    account,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/", expense_view, name="category_list"),
    path("accounts/", accounts_view, name="account_list"),
    path("categories/create/", expense_category, name="create_category"),
    path("accounts/create/", account, name="create_account"),
]
